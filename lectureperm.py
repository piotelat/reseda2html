#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep  4 15:42:00 2019
Lecture du fichier des permanents
@author: zabeth

On veut obtenir ça : 
<ul class="people">
    <li class="nom">
       <a href="https://perso.limsi.fr/zabeth/">Elisabeth PIOTELAT </b></a>
    </li>
    <li class="tel">01 69 15 8258</li>
    <li class="groupe">AMIC - Administration des Moyens Informatiques Communs</li>
</ul>

"""
import csv
import configparser
from datetime import datetime
import affichageweb

config = configparser.RawConfigParser()
config.read ('fichiers.cfg')

      
# Cree des petits fichiers json à partir du fichier permanent créé à la main
# Ne sert qu'une fois
def recupperm(csvperm,jsondir):
   with open(csvperm, newline='') as csvfilea:
       lecteur=csv.reader(csvfilea, delimiter=';')
       for individu in lecteur:
          nom = individu [0]
          jsonfic = jsondir + "/" + nom + ".json"
          jsonfic = jsonfic.replace(" ","")
          tel = individu [3]
          web = individu [4]
          creejson(jsonfic,tel,web)
          
          
# Cree des fichiers json          
def creejson(fichiernom,tel,web):
    f = open(fichiernom,"w",encoding="utf8")
    print ("\n\n"+fichiernom+"\n")
    texte = "{\n";
    texte += "\"Tel\":\""+tel+"\",\n"
    texte += "\"Web\":\""+web+"\",\n"
    texte += "\"Photo\":\"\"\n"
    texte +="}\n"
    f.writelines (texte)
          
# Où sont les fichiers à traiter
racine=config.get('CONF','racine')
perm = config.get('EXPORT','permanents')
tag = config.get('ANCRE','permanents')

# Où sont les informations spécifiques aux utilisateurs
infouser = config.get('CONF','info')

#Où doit-on écrire ? 
sortie = config.get('CONF','sortie')
permh = config.get('OUT','permanents')

maintenant = datetime.now()
# %d : jour du mois
# %m : mois
# %Y : année sur 4 chiffres
datefr= datetime.strftime(maintenant,"%d/%m/%Y")

# Permanents 
fich = racine + "/" + perm
html = sortie + "/" + permh
liste=affichageweb.entreeul (fich,infouser,tag)


fhtml = open(html,"w",encoding="utf8")
fhtml.write ("<h3>Permanents au "+datefr+"</h3>\n\n")
fhtml.writelines(liste)
fhtml.close

