#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Lecture des doctorants. 
Appelé une ou deux fois par an
"""

import configparser
from datetime import datetime
import affichageweb

config = configparser.RawConfigParser()
config.read ('fichiers.cfg')

# Où sont les fichiers à traiter
racine=config.get('CONF','racine')
doc = config.get('EXPORT','doctorants')

# Où sont les informations spécifiques aux utilisateurs
infouser = config.get('CONF','info')

#Où doit-on écrire ? 
sortie = config.get('CONF','sortie')
doch = config.get('OUT','doctorants')

maintenant = datetime.now()
# %d : jour du mois
# %m : mois
# %Y : année sur 4 chiffres
datefr= datetime.strftime(maintenant,"%d/%m/%Y")

# Doctorants
fich = racine + "/" + doc
html = sortie + "/" + doch

fdhtml = open(html,"w",encoding="utf8")
fdhtml.write ("<h3>Doctorants au "+datefr+"</h3>\n\n")
liste=affichageweb.entreeul (fich,infouser,"non")
fdhtml.writelines(liste)
fdhtml.close

